import tensorrt as trt
import uff
TRT_LOGGER= trt.Logger(trt.Logger.WARNING)
model_file= uff.from_tensorflow_frozen_model("./save_graph/frozen_Pnet_valid.pb",["Softmax","conv4-2/conv4-2"])
max_batch_size = 3

with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.UffParser() as parser:

    parser.register_input("Placeholder", (3, 12, 12))
    parser.register_output("Softmax")
    parser.register_output("conv4-2/conv4-2")
    #parser.register_output("conv6-3/conv6-3")
    # parser.register_output("conv4-2/conv4-2")
    #parser.register_output("bbox_ped_end/Relu")
    parser.parse_buffer(model_file, network)
    builder.max_batch_size = max_batch_size
    print(builder.platform_has_fast_int8)
   # assert False
    #builder.int8_mode = True
    builder.max_workspace_size = 1 <<  20 # This determines the amount of memory available to the builder when building an optimized engine and should generally be set as high as possible.
    print("done optimizer")
    with builder.build_cuda_engine(network) as engine:
        #serialized_engine = engine.serialize()
        
        with open('./save_engine/Pnet.engine', 'wb') as f:
            f.write(engine.serialize())
print("done")

# with trt.Builder(TRT_LOGGER) as builder:
#     with builder.build_cuda_engine(network) as engine:
#         serialized_engine = engine.serialize()
#         with open('sample.engine', 'wb') as f:
#             f.write(engine.serialize())



# with trt.Builder(TRT_LOGGER) as builder, builder.create_network() as network, trt.OnnxParser(network, TRT_LOGGER) as parser:
#     with open(model_path, 'rb') as model:
#         parser.parse(model.read())
#         builder.max_batch_size = max_batch_size
#         builder.platform_has_fast_int8 = True
#         builder.int8_mode = True
#         builder.max_workspace_size = 1 <<  20 # This determines the amount of memory available to the builder when building an optimized engine and should generally be set as high as possible.
# with trt.Builder(TRT_LOGGER) as builder:
#     with builder.build_cuda_engine(network) as engine:
#         serialized_engine = engine.serialize()
# with trt.Runtime(TRT_LOGGER) as runtime:
#     engine = runtime.deserialize_cuda_engine(serialized_engine)
#     with open('sample.engine', 'wb') as f:
#         f.write(engine.serialize())
