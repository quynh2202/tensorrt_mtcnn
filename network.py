import tensorflow as tf
import os
def conv(name , x, filter_size_h,filter_size_w, out_filters, stride_h=1,stride_w=1, group=1, biased=True, padding="VALID"):
    in_filters = int(x.get_shape()[-1])
    with tf.variable_scope(name) as scope:
        # kernel = tf.get_variable(name, shape, trainable=self.trainable)
        kernel = tf.get_variable('weights', shape=[filter_size_h, filter_size_w,in_filters, out_filters ] )
        # This is the common-case. Convolve the input without any further complications.
        output = tf.nn.conv2d(x,kernel,[1,stride_h,stride_w,1],padding=padding)
        # Add the biases
        if biased:
            biases = tf.get_variable('biases', [out_filters])
            output = tf.nn.bias_add(output, biases)
        if relu:
            # ReLU non-linearity
            output = tf.nn.relu(output, name=scope.name)
    return output
    # with tf.variable_scope(name):
    #     n = filter_size * filter_size * out_filters
    #     filter = tf.get_variable('DW', [filter_size, filter_size, in_filters, out_filters], tf.float32, tf.random_normal_initializer(stddev=0.01))
    #     # print(x.get_shape().as_list())
    #     # print(filter.get_shape().as_list())
    #     return tf.nn.conv2d(x, filter, [1, strides, strides, 1], 'SAME')

def relu(x):
    return tf.nn.relu(x)

def max_pool(x, filter, strides, name=None,padding="SAME"):
    return tf.nn.max_pool(x, ksize=[1, filter, filter, 1], strides=[1, strides, strides,1], padding=padding,name=name)
# @layer
def fc(inp, num_out, name, relu=True):
    with tf.variable_scope(name):
        input_shape = inp.get_shape()
        if input_shape.ndims == 4:
            # The input is spatial. Vectorize it first.
            dim = 1
            for d in input_shape[1:].as_list():
                dim *= int(d)
            feed_in = tf.reshape(inp, [-1, dim])
        else:
            feed_in, dim = (inp, input_shape[-1].value)
        weights = tf.get_variable('weights', shape=[dim, num_out])
        print("weight"+name,weights.get_shape())
        biases = tf.get_variable('biases', [num_out])
        op = tf.nn.relu_layer if relu else tf.nn.xw_plus_b
        fc = op(feed_in, weights, biases, name=name)
        return fc
def softmax(target,name=None):
    return tf.nn.softmax(target, name=name)
    # max_axis = tf.reduce_max(target, axis, keepdims=True)
    # target_exp = tf.exp(target-max_axis)
    # normalize = tf.reduce_sum(target_exp, axis, keepdims=True)
    # softmax = tf.div(target_exp, normalize, name)
    # return softmax


def Network(x):
    x = conv('conv1', x, 3, 3, 10, 1,1)
    print("conv1", x.get_shape())
    x = relu(x)
    x = max_pool(x, 2, 2)
    print("max1",x.get_shape())
    # print(x.get_shape().as_list())
    x = conv('conv2', x, 3, 3, 16, 1,1)
    print("conv2", x.get_shape())
    x = relu(x)
    #x = max_pool(x, 2, 2)
    x = conv('conv3', x, 3, 3, 32, 1,1)
    print("con3", x.get_shape())
    x_in = relu(x)
    #x = max_pool(x, 2, 2)
    x = conv('conv4-1', x, 1, 1, 2, 1,1)
    print("cnv4-1",x.get_shape())
    x=  softmax(x,name="prob1")
    print("sm",x.get_shape())
    #x = max_pool(x, 2, 2)
    x = conv('conv4-2', x_in, 1,1,4,1,1)
    print(x.get_shape())
    # assert False
    #x = max_pool(x, 2, 2)
    x = tf.reshape(x,[-1,4],"reshape")
    return x
def NetworkR(x):
    x = conv("conv1",x,3,3,28,1,1)
    print(x.get_shape())
    x = relu(x)
    x = max_pool(x,3,2,name="pool1")
    print(x.get_shape())
    x = conv("conv2",x,3,3,48,1,1)
    print(x.get_shape())
    x = relu(x)
    x = max_pool(x,3,2,name="max_pool2",padding="VALID")
    print(x.get_shape())
    x = conv("conv3",x,2,2,64,1,1)
    print(x.get_shape())
    x = relu(x)
    x = fc(x,128,relu=False,name="conv4")
    print(x.get_shape())
    x_in = relu(x)
    x = fc(x_in,2,relu=False,name="conv5-1")
    print(x.get_shape())
    x = softmax(x, name="prob1")
    print(x.get_shape())
    x = fc(x_in,4, name="conv5-2")
    print(x.get_shape())
    return x
def NetworkO(x):
    x = conv("conv1",x,3,3,32,1,1)
    x = relu(x)
    x = max_pool(x,3,2,name="poo1")
    x = conv("conv2",x,3,3,64,1,1)
    x = relu(x)
    x= max_pool(x,3,2,name="max_pool2", padding="VALID")
    x = conv("conv3",x,3,3,64,1,1)
    x = relu(x)
    x = max_pool(x,2,2,name="pool3")
    x = conv("conv4",x,2,2,128,1,1)
    x = relu(x)
    x = fc(x,256,name="conv5")
    x_in = relu(x)
    
    x = fc(x_in,2,name="conv6-1")
    x = softmax(x,name="prob1")
    x = fc(x_in,4,name="conv6-2")
    x = fc(x_in,10,name="conv6-3")
    return x
sess = tf.InteractiveSession()
global_step = tf.contrib.framework.get_or_create_global_step()

x = tf.placeholder(tf.float32, [None, 48, 48, 3])
x = NetworkO(x)
import numpy as np

data_path = '/home/tinh/face-master/facenet-master/src/align/det3.npy'
w = []
data_dict = np.load(data_path, encoding='latin1').item()
for op_name in data_dict:
    # print("name",op_name)
    if op_name.startswith("conv") :
        
        with tf.variable_scope(op_name, reuse=True):
            # Python3: dict has no module iteritems
            print(op_name)
            for param_name, data in data_dict[op_name].items():
               
                print(param_name)
                var = tf.get_variable(param_name)
                print("data",data.shape)
            
                print(var.get_shape().as_list())
                sess.run(var.assign(data))
                
saver = tf.train.Saver()
sess.run(tf.global_variables_initializer())
output_path = "Onet/Onet.ckpt"

save_path = saver.save(sess, output_path)
print("done")





assert False
for op_name in data_dict:
    # print("name",op_name)
    if op_name.startswith("conv"):
        # print(op_name)
        # print(op_name+"/weights")
        # var = tf.get_variable(op_name+"/weights")
        

        # assert False
        # sess.run(var.assign(data))
        # assert False
        # print(op_name)
        with tf.variable_scope(op_name, reuse=True):
            # Python3: dict has no module iteritems
            print(op_name)
            for param_name, data in data_dict[op_name].items():
                # print(param_name)
                # assert False
                # print(data)
                # for var in tf.trainable_variables():
                #     # print(var.op.name)
                print(param_name)
                var = tf.get_variable(param_name)
                print("data",data.shape)
            
                # print(data)
                # assert False
                # for dt in data.items():
                #     print(dt)
                # print(data["biases"])
                print(var.get_shape().as_list())
                sess.run(var.assign(data))
                # print(data)
                # print("dfj,bhkfgb")
                # print(var.eval())
                # assert False
                #     if var.name.find('conv')>0:
                #         print("vao")
                #         # print()
                #         # print(data)
                #         var.assign(data["weights"])
                #         print(var.eval())
                #         print("vao")
                #         assert False
                # if param_name.startswith('conv'):
                # try:
                #     print(param_name,data)
                #     # var = tf.get_variable(param_name)
                #     sess.run(var.assign(data))
                #     # print(sess.run(var))
                #     # print(var.eval())
                # except ValueError:
                #     if not ignore_missing:
                #         raise
saver = tf.train.Saver()
# var =  tf.get_variable("conv1")
sess.run(tf.global_variables_initializer())

# print(sess.run(var))
output_path = "Rnet_valid/Pnet.ckpt"
# print()
save_path = saver.save(sess, output_path)
print("done")



