import tensorrt as trt
import pycuda.driver as cuda
import pycuda.autoinit
import numpy as np
import cv2
import time
TRT_LOGGER = trt.Logger(trt.Logger.WARNING)
# load file engine
with open('./save_engine/Rnet.engine', 'rb') as f, trt.Runtime(TRT_LOGGER) as runtime:
    engine = runtime.deserialize_cuda_engine(f.read())
#print('engine',engine)
#assert False
# cap bo nho
#h_input = cuda.pagelocked_empty(engine.get_binding_shape(0).volume(), dtype=np.float32)
#h_output = cuda.pagelocked_empty(engine.get_binding_shape(1).volume(), dtype=np.float32)
# Allocate device memory for inputs and outputs.
img = cv2.imread("3.jpg")
img = cv2.resize(img,(48,48))
img = img.astype(np.float32)
img2 = cv2.imread("1.jpg")
img2 = cv2.resize(img2,(48,48))
img_test = np.array([img,img2])
#img = img - img
# img = cv2.resize(img,(48,48))
print(img.shape)
#img = img - img
t1 = time.time()
h_input = img.astype(np.float32)
batch_size =1
h1_output = np.empty(batch_size*2, dtype = np.float32)
h2_output = np.empty(batch_size*4, dtype = np.float32)
# h3_output = np.empty(batch_size*4, dtype = np.float32)
d_input = cuda.mem_alloc(batch_size*img.nbytes)
d1_output = cuda.mem_alloc(h1_output.nbytes)
d2_output = cuda.mem_alloc(h2_output.nbytes)
# d3_output = cuda.mem_alloc(h3_output.nbytes)
# Create a stream in which to copy inputs/outputs and run inference.
stream = cuda.Stream()

#  chay tren cuda
print(engine.create_execution_context())
with engine.create_execution_context() as context:
    # Transfer input data to the GPU.
    cuda.memcpy_htod_async(d_input, h_input, stream)
    
    # Run inference.
    print(context.execute_async(bindings=[int(d_input), int(d1_output),
        int(d2_output)],
            stream_handle=stream.handle))
    # Transfer predictions back from the GPU.
    cuda.memcpy_dtoh_async(h1_output, d1_output, stream)
    cuda.memcpy_dtoh_async(h2_output, d2_output, stream)
    # cuda.memcpy_dtoh_async(h3_output, d3_output, stream)
    print(h1_output)
    print(h2_output)
    # print(h3_output)
    # Synchronize the stream
    stream.synchronize()
t2= time.time()
print(t2-t1)
    # Return the host output.
# return h_output
